#! /bin/bash

#Version: 1.2
# Date:- 24/07/2022

# Auto CronJob addition
# Changable
auto_cron=0
# Bool value; will turn 1 when mail and password are deemed valid
mail_valid=0
pass_valid=0
# Number of total attempt
# Changable
attempt=2
insert_check=0

# install Directory setup
# Changable
install_dir="/opt/mailNotify"
# Base directory of the script
pDir=$(dirname ${BASH_SOURCE[0]})
# this will turn into password.py
# password.py can't be stored in git for security reasons
template_file="$pDir/src/pass_file_template"
temp_file="$pDir/src/password_temp"


insert_value_in_file(){
	# insert_value_in_file "hook-strings" "email/password value"
	if [[ "$2" =~ "#" ]]; then
		sed -i "s/$1/$2/" "$temp_file"
	elif [[ "$2" =~ "/" ]]; then
		sed -i "s#$1#$2#" "$temp_file"
	elif [[ "$2" =~ "#" && "$2" =~ "/" ]]; then
		printf "$1: Needs to be inserted manually\n"
		insert_check=1
	else
		sed -i "s/$1/$2/" "$temp_file"
	fi
}


get_email(){
	# only have 2 chance to enter correct mail
	local mail_attem=$attempt

	printf "\nOnly Gmail Works for now\n"

	while [ $mail_attem -gt 0 ]; do
		read -p "Enter gmail address: " mailAdd

		if [[ "$mailAdd" =~ [a-zA-Z0-9]"@gmail.com" ]]; then
			printf "\nMail address: %s\n" $mailAdd
			read -p "Are you sure?[y/n]: " confirm

			case $confirm in
				Y|y|yes|Yes)
					mail_valid=1
					break
					;;
				N|n|no|No|NO) printf "\nTry Again\n";;
				*)
					printf "Input Not Recognised\n"
					exit;;
			esac
			((mail_attem--))

		else
			printf "\n\tNeeds to be a gmail address"
			printf "\n\tWill include other services in the future\n"
			exit

		fi

		if [ $mail_attem -eq 0 ]; then
			printf "No Email address given\n"
			exit
		fi
	done

}

get_password(){
	local pass_attem=$attempt
	local pass_check=0

	while [ $pass_attem -gt 0 ]; do
		read -p "Enter password: " -s passAdd
		printf "\n"
		read -p "Re-enter password:" -s rePassAdd
		printf "\n\n"

		if [ -z "$passAdd" ]; then
			#PassWord is empty; Try again
			pass_check=3
		elif [[ "$passAdd" != "$rePassAdd" ]]; then
			#doesn't match
			pass_check=2
		else
			#matches
			pass_check=1
		fi

		case $pass_check in
			1)
				pass_valid=1
				break
				;;
			2) printf "PassWords didn't match; Try again\n" ;;
			3) printf "PassWords can't be empty; Try again\n" ;;
			0)
				printf "\nSomething wrong with script\n"
				exit
				;;
		esac
		((pass_attem--))
		# Last chance warning
		[ $pass_attem -eq 1 ] && printf "Last chance to get it correct\n"
	done

}

## Start
# Directory Check
[[ -d "$pDir" ]] || ( echo $pDir; printf "Repo Dir not found\n"; exit)
# Create installation dir
[ -d "$install_dir" ] || sudo mkdir -p "$install_dir"

[[ -f "$temp_file" ]] || cp $template_file "$temp_file"


# if the placeholder values haven't been tampered with, call the functions
if [[ $(grep -c "mail_value" "$temp_file") -eq 1 ]] &&\
	[[ $(grep -c "pass_value" "$temp_file") -eq 1 ]]; then
	get_email
	get_password
else
	printf "\n\t1 or both hooks are missing; insert manually\n"
fi


# Start writing only when both of the values are acceptable
if [ $mail_valid -eq 1 ] && [ $pass_valid -eq 1 ]; then
	insert_value_in_file "mail_value" "$mailAdd"
	insert_value_in_file "pass_value" "$passAdd"
	if [ $insert_check -eq 0 ]; then
		printf "Both Values have been placed\n"
	else
		printf "Manual Attention Needed\n"
	fi
fi

# Transporting The files in installed folder
sudo cp "$temp_file" "$install_dir"/password.py
sudo cp "$pDir"/src/mailNotify.py	"$pDir"/README.md "$install_dir"/

printf "\nInstallation: Done\n"

cronjob_setup(){
	# Add a cronjob to run the script every 2 Hours
	(crontab -l; cat cronjob_setup ) | crontab -
}

# For String flags '-n & -z' it needs to be double bracket [[  ]]
case $auto_cron in
	0)
		if [[ -n $(which crontab) ]] && [[ $(crontab -l | grep -c "mailNotify") -eq 0 ]]; then
			cronjob_setup
			printf "Cronjob SetUp: complete\n"
		fi
		;;
	*) printf "Cronjob SetUp: skipped\n"
		;;
esac

# If temp file still exists; delete it for security
printf "Cleaning ........\n"
[[ -f "$temp_file" ]] && rm "$temp_file"

printf "Install: Complete\n"
